# io_scene_bfres

This is a Blender add-on to import the BFRES model file format, which is typically used in several Nintendo Wii U games.

![alt tag](https://gitlab.com/Syroot/io_scene_bfres/raw/master/doc/readme/example.png)

S. the wiki for [help and more information](https://gitlab.com/Syroot/io_scene_bfres/wikis).

Please not that it is based on _very_ early BFRES research and has never been rewritten after the format was completely documented. It only works "good enough" for Mario Kart 8 models. This project is now archived.
